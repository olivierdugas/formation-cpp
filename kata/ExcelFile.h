#ifndef EXCELFILE_H
#define EXCELFILE_H

#include <QString>

namespace kata
{

class ExcelFile
{
public:
    virtual ~ExcelFile(){}
    virtual QString read(const QString& cell) const = 0;
    virtual void write(const QString& value, const QString cell) const = 0;
    virtual QString offset(const QString& cell, const int rows, const int columns) const = 0;
};

}

#endif
