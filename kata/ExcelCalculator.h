#pragma once

#include "ExcelFile.h"

namespace kata
{

class ExcelCalculator
{
public:
    ExcelCalculator(const ExcelFile& excelFile);

private:
    const ExcelFile& excelFile;
};

}
