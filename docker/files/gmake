#!/usr/bin/env bash
# The script will search in every cpp files, which are potentially test files,
# to find inclusion of mocks. When one is found, this will trigger the mock's
# header generation.
SOURCES_DIR=${1:-"$(pwd)"}  #This arcane syntax means: if argument 1 is not provided, use ${pwd}

all_sources="$(mktemp)"
find "$SOURCES_DIR/applications" "$SOURCES_DIR/libraries" "$SOURCES_DIR/products" -type f > "$all_sources"

TEST_SOURCE_FILES=$(grep -E "\.cpp$" "$all_sources" | xargs grep -l "Mock\.h")

RegularIFS=$IFS
NewLineIFS=$(echo -en "\n\b")

# When calling hasMoreThanOneName, the argument should be quoted to prevent bash
# from splitting it into multiple strings: "$argument" and not $argument
function hasMoreThanOneName
{
  whiteSpaceCharacterCount=$(echo $1 | tr -cd ' \t' | wc -c)
  if [ $whiteSpaceCharacterCount -gt 0 ]; then
    echo "true"
  else
    echo "false"
  fi
}

function mockFiles
{
    # Scan in every test files
    for sourceFile in ${SOURCE_FILES}; do
	    prefix="$(dirname $sourceFile | awk -F"$testtype" '{print $1}')$testtype/"

	    # Every include line ending with "Mock.h" is the keyword for a mock inclusion.
	    # Note that this script currently does not support hpp headers.
	    # Each matching line will be parsed to extract the expected file name of the
	    # desired header to mock.
	    IFS=$NewLineIFS
	    for newMock in $(grep "#include.*Mock.h" $sourceFile); do
		    fileExt="$(echo $newMock | awk -F'[<\\>\"]' '{print $2}' | sed 's/.*\.//')"
		    newMock="$(echo $newMock | awk -F'[<\\>\"]' '{print $2}' | sed 's/\.h[xp]*$//')"
		    mockFilename="$(basename $newMock)"
		    dirName=$(dirname $newMock)
		    INTERFACE_TO_MOCK="${newMock%Mock}.$fileExt"

            if echo "$mocked" | grep -q "$newMock"; then
		        #echo "$newMock is already mocked! Continuing..."
		        continue
		    else
		        mocked="$mocked $newMock"
		    fi

		    header=$(grep "${INTERFACE_TO_MOCK}" "$all_sources")


		    if $(hasMoreThanOneName "$header"); then
		        printf "***\nError! while generating $newMock.$fileExt found in\n$sourceFile\n"
		        printf "multiple header files were found with the same name ${INTERFACE_TO_MOCK}:\n$header\n"
		        printf "Try using a relative path in your include statement, e.g.\n"
		        printf "#include \"rqcommunication/rqmodbusabstractMock.h\" rather than\n"
		        printf "#include \"rqmodbusabstractMock.h\".\n***\n"
		        return 1
		    fi

		    if [ -z "$header" ]; then
		        echo "Error! File to mock was not found: ${INTERFACE_TO_MOCK}"
		        return 1
		    fi

		      $SOURCES_DIR/scripts/gmock/mmock "$header" "$dirName" "$mockFilename.$fileExt" "$prefix"
	    done
	    IFS=$RegularIFS
    done
    return 0
}

SOURCE_FILES="$TEST_SOURCE_FILES"
testtype="test"
mocked=""
mockFiles
exit $?

